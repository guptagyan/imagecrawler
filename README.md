# ImageCrawler
    
    * Simple image crawler app to download all images from a given url/website/webpage.

# Instalation and Working structure:-

- Create a virtualenv with python3 as default python, activate it and run the following  commands in terminal

- Go to project directory using terminal.

- Run `pip install -r requirements.txt`

- Run `python image_crawler.py`

- Finally images will be downloaded and stored in foldder named (downloaded-images)